# 合并验证集和训练集的Entity数据，并将Entity格式化到目标格式
import json

# 加载jsonl数据
def read_jsonl_to_Dict(jsonl_file: str):
    with open(jsonl_file) as f:
        lines = f.read().splitlines()
    id_dicts = {}

    for line in lines:
        line_dict = json.loads(line)
        if isinstance(line_dict["id"], int):
            # 是数字的话补为5位的字符串，与其他数据兼容
            id_dicts[str(line_dict["id"]).zfill(5)] = line_dict
        else:
            id_dicts[line_dict["id"]] = line_dict
    return id_dicts

entity_data_1 = read_jsonl_to_Dict("preprocess/add_data/train_dev_all.entity.jsonl")
entity_data_2 = read_jsonl_to_Dict("preprocess/add_data/test_seen.entity.jsonl")
entity_data_3 = read_jsonl_to_Dict("preprocess/add_data/test_unseen.entity.jsonl")

merged_entity_data = {**entity_data_1, **entity_data_2, **entity_data_3}

# 格式化字典到新格式，只保留entity字段
processed_entity_data = {}
for _id in merged_entity_data:
    flattened_entities = []
    entity_list = merged_entity_data[_id]["partition_description"]

    # 展平一些多维列表
    for item in entity_list:
        if isinstance(item, list):
            flattened_entities.extend(item)
        else:
            flattened_entities.append(item)

    # 格式化
    processed_entity_data[_id] = " ".join(flattened_entities)

# 保存到新文件备用
with open("preprocess/processed_data/entity.json", "w") as f:
    json.dump(processed_entity_data, f, indent=4)

print("处理成功!")