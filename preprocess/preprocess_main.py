# 为新的验证集和测试集生成domain_splits下的json文件
# 需要前置运行preprocess_entity.py和preprocess_race.py
import json
import pickle as pkl
import pandas as pd
from transformers import BertTokenizer

# 加载jsonl数据
def read_jsonl_to_Dict(jsonl_file: str):
    with open(jsonl_file) as f:
        lines = f.read().splitlines()
    id_dicts = {}

    for line in lines:
        line_dict = json.loads(line)
        if isinstance(line_dict["id"], int):
            # 是数字的话补为5位的字符串，与其他数据兼容
            id_dicts[str(line_dict["id"]).zfill(5)] = line_dict
        else:
            id_dicts[line_dict["id"]] = line_dict
    return id_dicts

# 实例化tokenizer
tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

# 加载Entity
entity_data = json.load(open("preprocess/processed_data/entity.json"))

# 加载Race
race_data = json.load(open("preprocess/processed_data/race.json"))

# 加载Dataset的数据
dataset_name_list = ["dev", "test_seen", "test_unseen"]

for dataset_name in dataset_name_list:
    dataset = read_jsonl_to_Dict(f"preprocess/dataset/{dataset_name}.jsonl")

    # 处理不重要的字段
    for _id in dataset:
        dataset[_id]["attack"] = [0, 0, 0, 0, 0]
        dataset[_id]["org_sent"] = dataset[_id]["text"]

    # 处理clean_sent、entity和race字段
    for _id in dataset:
        dataset[_id]["clean_sent"] = dataset[_id]["text"]
        dataset[_id]["entity"] = entity_data[_id]
        dataset[_id]["race"] = race_data[_id]

    # 处理要编码的字段
    for _id in dataset:
        dataset[_id]["ent_tokens"] = tokenizer(dataset[_id]["entity"]).input_ids
        dataset[_id]["bert_tokens"] = tokenizer(dataset[_id]["clean_sent"]).input_ids
        dataset[_id]["race_tokens"] = tokenizer(dataset[_id]["race"]).input_ids
    
    # 去掉原有的"id"列和"text"列，处理img字段，并将item保存为列表形式导出
    processed_dataset = []
    for _id in dataset:
        dataset[_id].pop("id")
        dataset[_id].pop("text")
        dataset[_id]["img"] = dataset[_id]["img"].replace("img/", "")

        processed_dataset.append(dataset[_id])
    
    # 导出数据集
    with open(f"data/domain_splits/mem_{dataset_name}.json", "w") as f:
        json.dump(processed_dataset, f)

print("导出成功!")