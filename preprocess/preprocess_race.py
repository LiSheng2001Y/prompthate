# 提取种族数据，并格式化到目标格式
import json

race_data = json.load(open("preprocess/add_data/box_annos.race.json"))

processed_race_data = {}
for item in race_data:
    _id = item["img_name"].replace(".png", "")
    item_race_list = []
    for box in item["boxes_and_score"]:
        if box["race"] is not None:
            item_race_list.append(f'{box["gender"]} {box["race"]}')

    processed_race_data[_id] = " ".join(item_race_list) if len(item_race_list)>0 else "unk"


# 保存到新文件备用
with open("preprocess/processed_data/race.json", "w") as f:
    json.dump(processed_race_data, f, indent=4)

print("处理成功!")
